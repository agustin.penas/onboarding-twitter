package twitterkata.infrastructure

import org.junit.After
import org.junit.Test
import twitterkata.core.domain.Tweet
import twitterkata.core.domain.TweetRepository
import java.io.File

abstract class TweetRepositoryTest {


    private lateinit var tweets: List<String>
    protected lateinit var tweetRepository: TweetRepository
    private val daroNick = "Daro"
    private var DAROSTWEET = Tweet(daroNick, "@Daro la rompe toda")
    private var SOSASTWEET = Tweet("Sosa", "@Sosa la rompe toda")
    private var OTHER_DAROSTWEET = Tweet(daroNick, "@Sosa la rompe toda")


    abstract fun createRepository(): TweetRepository

    @After
    abstract fun tearDown()

    @Test
    internal fun `a tweet can be added and retrieved`() {
        givenARepositoryWithTweets(listOf(DAROSTWEET))

        whenLookingForUserTweets(daroNick)

        thenExpectedTweetIs(DAROSTWEET.tweetText)
    }

    @Test
    fun `returns empty list when user does not exists`() {
        givenAnEmptyRepository()

        whenLookingForUserTweets(daroNick)

        thenThereAreNoTweets()
    }

    @Test
    fun `past saved tweets persist`() {
        givenARepositoryWithTweets(listOf(DAROSTWEET, SOSASTWEET))

        whenLookingForUserTweets(daroNick)

        thenExpectedTweetIs(DAROSTWEET.tweetText)
    }

    @Test
    fun `users can have multiple tweets`() {
        givenARepositoryWithTweets(listOf(DAROSTWEET, SOSASTWEET, OTHER_DAROSTWEET))

        whenLookingForUserTweets(daroNick)

        thenExpectedTweetIs(DAROSTWEET.tweetText)
        thenExpectedTweetIs(OTHER_DAROSTWEET.tweetText)
    }

    private fun givenAnEmptyRepository() {
        tweetRepository = createRepository()
    }

    private fun givenARepositoryWithTweets(tweets: List<Tweet>) {
        tweetRepository = createRepository()
        for (tweet in tweets) tweetRepository.save(tweet)
    }

    private fun whenLookingForUserTweets(nick: String) {
        tweets = tweetRepository.find(nick)
    }

    private fun thenExpectedTweetIs(expectedTweet: String) {
        assert(tweets.contains(expectedTweet))
    }

    private fun thenThereAreNoTweets() {
        assert(tweets.isEmpty())
    }
}

class FileTweetRepositoryTest : TweetRepositoryTest() {
    private val fileName = "src/test/resources/tweets"
    override fun createRepository(): TweetRepository {
        return FileTweetRepository(fileName)
    }

    override fun tearDown() {
        val file = File(fileName)
        if (file.exists()) file.delete();
    }

}

