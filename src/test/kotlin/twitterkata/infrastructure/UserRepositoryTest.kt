package twitterkata.infrastructure

import org.junit.After
import org.junit.Test
import twitterkata.core.domain.User
import twitterkata.core.domain.UserRepository
import java.io.File

abstract class UserRepositoryTest {


    protected lateinit var userRepository: UserRepository
    private var user: User? = null
    private var DARO = User("Marcos Dario Pizzio", "@Daro", mutableSetOf("@Sosa"))
    private var DARO_MODIFICADO = User("Dario Pizzio", "@Daro")
    private var SOSA = User("Manuel Sosa", "@Sosa")

    abstract fun createRepository(): UserRepository

    @After
    abstract fun tearDown()

    @Test
    internal fun `a user can be added and retrieved`() {
        givenARepositoryWithUsers(listOf(DARO))

        whenLookingForUser(DARO.nick)

        thenExpectedUserIs(DARO)
    }

    @Test
    fun `returns null when user does not exists`() {
        givenAnEmptyRepository()

        whenLookingForUser(DARO.nick)

        thenExpectedUserIsNotFound()
    }

    @Test
    fun `past saved users persist`() {
        givenARepositoryWithUsers(listOf(DARO, SOSA))

        whenLookingForUser(DARO.nick)

        thenExpectedUserIs(DARO)
    }

    @Test
    fun `users can be updated`() {
        givenARepositoryWithUsers(listOf(DARO, DARO_MODIFICADO))

        whenLookingForUser(DARO.nick)

        assert(DARO_MODIFICADO == user)
    }

    private fun givenAnEmptyRepository() {
        userRepository = createRepository()
    }

    private fun givenARepositoryWithUsers(users: List<User>) {
        userRepository = createRepository()
        for (user in users) userRepository.save(user)
    }

    private fun whenLookingForUser(nick: String) {
        user = userRepository.find(nick)
    }

    private fun thenExpectedUserIs(expectedUser: User) {
        assert(user == expectedUser)
    }

    private fun thenExpectedUserIsNotFound() {
        assert(user == null)
    }
}

class InMemoryUserRepositoryTest : UserRepositoryTest() {

    override fun createRepository(): UserRepository {
        return InMemoryUserRepository()
    }

    override fun tearDown() {
    }

}

class FileUserRepositoryTest : UserRepositoryTest() {
    private val fileName = "src/test/resources/users"
    override fun createRepository(): UserRepository {
        return FileUserRepository(fileName)
    }

    override fun tearDown() {
        val file = File(fileName)
        if (file.exists()) file.delete();
    }

}

