package twitterkata.core.action

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import twitterkata.core.domain.*
import twitterkata.infrastructure.InMemoryUserRepository

class GetTweetsTest {

    private val DARIO = User("Dario Pizzio", "@Daro")
    private var tweetRepository: TweetRepository = mock {}
    private var userRepository = InMemoryUserRepository()
    private val tweetString: String = "140 caracteres o algo asi"
    private val DARIOS_TWEET = Tweet(DARIO.nick, tweetString)
    private lateinit var obtainedTweets: List<String>


    @Test
    fun `returns empty list when no tweets where made`() {
        givenARepositoryWithAUser(DARIO)

        whenGettingTweets(DARIO.nick)

        thenThereAreNoTweets()
    }

    @Test
    fun `returns past tweets`() {
        givenRepositoriesWithUsersAndTweets(DARIO, DARIOS_TWEET.tweetText)

        whenGettingTweets(DARIO.nick)

        thenTheTweetsContains(DARIOS_TWEET.tweetText)
    }

    @Test(expected = UserNotFoundException::class)
    fun `fails when user is not found`() {

        whenGettingTweets(DARIO.nick)

        verify(userRepository).find(DARIO.nick)
    }

    private fun givenARepositoryWithAUser(user: User) {
        userRepository.save(user)
    }

    private fun givenRepositoriesWithUsersAndTweets(user: User, tweetText: String) {
        userRepository.save(user)
        tweetRepository = mock {
            on { find(user.nick) } doReturn listOf(tweetText)
        }
    }

    private fun whenGettingTweets(nickName: String) {
        obtainedTweets = GetTweets(tweetRepository, userRepository).execute(nickName)
    }

    private fun thenThereAreNoTweets() {
        assert(obtainedTweets == listOf<String>())
    }

    private fun thenTheTweetsContains(tweetText: String) {
        assert(obtainedTweets.contains(tweetText))
    }
}

