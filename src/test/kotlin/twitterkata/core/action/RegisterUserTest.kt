package twitterkata.core.action

import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import twitterkata.core.domain.User
import twitterkata.core.domain.UserAlreadyExistsException
import twitterkata.core.domain.UserRepository

class RegisterUserTest {


    private var userRepository: UserRepository = mock {}
    private var user = User("USERNAME", "NICK")

    @Test
    fun `a user can be registered successfully`() {

        givenARepositoryWithoutUsers()

        whenAddingAUser(user)

        thenRegisteredUserIs(user)
    }

    @Test(expected = UserAlreadyExistsException::class)
    fun `fails when user already exists`() {

        givenARepositoryWithAUser(user)

        whenAddingAUser(user)

        verify(userRepository).find(user.nick)
    }

    private fun givenARepositoryWithoutUsers() {
        userRepository = mock {
            on { find(any()) } doReturn null as User?
        }
    }

    private fun givenARepositoryWithAUser(user: User) {
        userRepository = mock {
            on { find(user.nick) } doReturn user
        }
    }

    private fun whenAddingAUser(user: User) {
        RegisterUser(userRepository).execute(user.realName, user.nick)
    }

    private fun thenRegisteredUserIs(expectedUser: User) {
        verify(userRepository).save(expectedUser)
    }

}

