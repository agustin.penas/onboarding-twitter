package twitterkata.core.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import twitterkata.core.domain.*
import twitterkata.infrastructure.InMemoryUserRepository

class DoTweetTest{


    private val DARIO = User("Dario Pizzio", "@Daro")
    private var tweetRepository: TweetRepository = mock {}
    private var userRepository = InMemoryUserRepository()
    private val tweetString: String = "140 caracteres o algo asi"
    private val DARIOS_TWEET = Tweet(DARIO.nick, tweetString)



    @Test
    internal fun `a user can tweet`() {

        givenARepositoryWithAUser()

        whenTweeting()

        verify(tweetRepository).save(DARIOS_TWEET)
    }

    @Test(expected = UserNotFoundException::class)
    fun `fails when user is not found`() {

        whenTweeting()

        verify(userRepository).find(DARIO.nick)
    }

    private fun givenARepositoryWithAUser() {
        userRepository.save(DARIO)
    }

    private fun whenTweeting() {
        DoTweet(tweetRepository, userRepository).execute(DARIO.nick, tweetString)
    }


}

