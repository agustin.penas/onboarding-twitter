package twitterkata.core.action

import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import twitterkata.core.domain.User
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class UpdateUserTest {

    private lateinit var userRepository: UserRepository

    @Test
    fun `updates real name successfully`() {
        givenARepositoryWithUser(User("Juan", "juan12"))

        whenUpdating(userRepository)

        val updatedUser = User("Carlitos", "juan12")

        verify(userRepository).save(updatedUser)
    }

    @Test(expected = UserNotFoundException::class)
    fun `fails when user is not found`() {
        givenARepositoryWithoutUsers()

        whenUpdating(userRepository)

        verify(userRepository, never()).save(any())
    }

    private fun givenARepositoryWithUser(user: User) {
        userRepository = mock { on { find(user.nick) } doReturn user }
    }

    private fun givenARepositoryWithoutUsers() {
        userRepository = mock { on { find(any()) } doReturn null as User? }
    }

    private fun whenUpdating(userRepository: UserRepository) {
        UpdateUser(userRepository)
            .execute(UpdateUser.ActionData("juan12", "Carlitos"))
    }
}

