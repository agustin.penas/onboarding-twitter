package twitterkata.core.action

import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import twitterkata.core.domain.User
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class GetFriendsTest {

    private val DARIO = User("Dario Pizzio", "@Daro")
    private val SOSA = User("Manuel Sosa", "@Sosa", mutableSetOf(DARIO.nick))
    private var userRepository = mock<UserRepository> {}

    @Test
    fun `returns the list of follows when asked`() {

        givenARepositoryWithUsers(listOf(SOSA, DARIO))

        val listOfFollows = whenFollowersAreRequested(SOSA)

        assert(listOfFollows.contains(DARIO.nick))
    }

    @Test(expected = UserNotFoundException::class)
    fun `fails when user is not found`() {
        givenARepositoryWithoutUsers()

        whenFollowersAreRequested(SOSA)

    }

    private fun givenARepositoryWithUsers(
        users: List<User>
    ) {
        userRepository = mock {}
        for (user in users) {
            userRepository.stub {
                on { find(user.nick) } doReturn user
            }
        }
    }

    private fun givenARepositoryWithoutUsers() {
        userRepository = mock {
            on { find(any()) } doReturn null as User?
        }
    }

    private fun whenFollowersAreRequested(user: User): Set<String> {
        return GetFriends(userRepository).execute(user.nick)
    }

}

