package twitterkata.core.action

import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import twitterkata.core.domain.User
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class FollowUserTest {

    private val DARIO = User("Dario Pizzio", "@Daro")
    private val SOSA = User("Manuel Sosa", "@Sosa")
    private val SOSA_FOLLOWING_DARIO = User("Manuel Sosa", "@Sosa", mutableSetOf<String>(DARIO.nick))

    private var userRepository: UserRepository = mock {}

    @Test
    fun `user can follow another user`() {

        givenARepositoryWithUsers(SOSA, DARIO)

        whenFollowingAnotherUser(SOSA, DARIO)

        verify(userRepository).save(SOSA_FOLLOWING_DARIO)
    }

    @Test(expected = UserNotFoundException::class)
    fun `fails when user is not found`() {
        givenARepositoryWithoutUsers()

        whenFollowingAnotherUser(SOSA, DARIO)

        verify(userRepository, never()).save(any())
    }


    private fun givenARepositoryWithUsers(
        oneUser: User,
        anotherUser: User
    ) {
        userRepository = mock {
            on { find(oneUser.nick) } doReturn User(oneUser.realName, oneUser.nick)
            on { find(anotherUser.nick) } doReturn User(
                anotherUser.realName,
                anotherUser.nick,
                mutableSetOf<String>(DARIO.nick)
            )
        }
    }

    private fun givenARepositoryWithoutUsers() {
        userRepository = mock {
            on { find(any()) } doReturn null as User?
        }
    }


    private fun whenFollowingAnotherUser(
        followerUser: User,
        userToFollow: User
    ) {
        FollowUser(userRepository)
            .execute(followerUser.nick, userToFollow.nick)
    }
}



