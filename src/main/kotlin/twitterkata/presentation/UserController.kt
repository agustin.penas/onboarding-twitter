package twitterkata.presentation

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import twitterkata.core.action.RegisterUser
import twitterkata.core.action.UpdateUser


@RestController
class UserController(private val registerUser: RegisterUser, private val updateUser: UpdateUser) {


    @PostMapping("/users")
    fun registerUser(@RequestBody userData: RegisterUserRequest): ResponseEntity<Any> {
        registerUser.execute(userData.name, userData.nick)
        return ResponseEntity.ok(HttpStatus.CREATED)
    }

    @PutMapping("/users")
    fun updateUser(@RequestBody userData: RegisterUserRequest): ResponseEntity<Any> {
        updateUser.execute(actionData = UpdateUser.ActionData(userData.nick, userData.name))
        return ResponseEntity.ok(HttpStatus.OK)
    }


    data class RegisterUserRequest(val nick: String, val name: String)
}