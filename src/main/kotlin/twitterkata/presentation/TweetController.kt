package twitterkata.presentation

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import twitterkata.core.action.DoTweet
import twitterkata.core.action.GetTweets

@RestController
class TweetController(private val doTweet: DoTweet, private val getTweets: GetTweets) {

    @GetMapping("/users/{userNick}/tweets")
    @ResponseBody
    fun getTweets(@PathVariable userNick: String): List<String> {
        return getTweets.execute(userNick)

    }

    @PutMapping("/users/{userNick}/tweets")
    @ResponseBody
    fun doTweet(@PathVariable userNick: String, @RequestBody tweetData: TweetRequest): ResponseEntity<Any> {
        doTweet.execute(userNick, tweetData.text)
        return ResponseEntity.ok(HttpStatus.OK)
    }

    data class TweetRequest(@JsonProperty("text") val text: String)
}