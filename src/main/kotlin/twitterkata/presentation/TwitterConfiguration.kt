package twitterkata.presentation

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import twitterkata.core.action.*
import twitterkata.core.domain.TweetRepository
import twitterkata.core.domain.UserRepository
import twitterkata.infrastructure.FileTweetRepository
import twitterkata.infrastructure.FileUserRepository

@Configuration
class TwitterConfiguration {
    @Bean
    fun createRegisterUser(userRepository: UserRepository): RegisterUser {
        return RegisterUser(userRepository)
    }

    @Bean
    fun createUpdateUser(userRepository: UserRepository): UpdateUser {
        return UpdateUser(userRepository)
    }

    @Bean
    fun createGetFriends(userRepository: UserRepository): GetFriends {
        return GetFriends(userRepository)
    }

    @Bean
    fun createFollowUser(userRepository: UserRepository): FollowUser {
        return FollowUser(userRepository)
    }

    @Bean
    fun createDoTweet(tweetRepository: TweetRepository, userRepository: UserRepository): DoTweet {
        return DoTweet(tweetRepository, userRepository)
    }

    @Bean
    fun createGetTweets(tweetRepository: TweetRepository, userRepository: UserRepository): GetTweets {
        return GetTweets(tweetRepository,userRepository)
    }

    @Bean
    fun createUserRepo(): UserRepository {
        return FileUserRepository("src/main/resources/users")
    }

    @Bean
    fun createTweetRepo(): TweetRepository {
        return FileTweetRepository("src/main/resources/tweets")
    }

}