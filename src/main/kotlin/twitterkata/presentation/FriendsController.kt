package twitterkata.presentation

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import twitterkata.core.action.FollowUser
import twitterkata.core.action.GetFriends


@RestController
class FriendsController(private val getFriends: GetFriends, private val followUser: FollowUser) {

    @GetMapping("/users/{userNick}/friends")
    @ResponseBody
    fun getFriends(@PathVariable userNick: String): Set<String> {
        return getFriends.execute(userNick)
    }

    @PutMapping("/users/{userNick}/friends")
    @ResponseBody
    fun followUser(@PathVariable userNick: String, @RequestBody friendData: FriendUserRequest): ResponseEntity<Any> {
        followUser.execute(userNick, friendData.nick)
        return ResponseEntity.ok(HttpStatus.OK)

    }

    data class FriendUserRequest(@JsonProperty("nick") val nick: String)
}