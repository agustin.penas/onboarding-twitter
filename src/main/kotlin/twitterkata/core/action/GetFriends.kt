package twitterkata.core.action

import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class GetFriends(private val userRepository: UserRepository) {
    fun execute(userWithFollows: String): Set<String> {

        return findUser(userWithFollows).getFriends()
    }

    private fun findUser(nickname: String) = userRepository.find(nickname) ?: throw UserNotFoundException(nickname)

}