package twitterkata.core.action

import twitterkata.core.domain.Tweet
import twitterkata.core.domain.TweetRepository
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class DoTweet(
    private val tweetRepository: TweetRepository,
    private val userRepository: UserRepository
) {
    fun execute(userNick: String, tweetText: String) {
        if(userNotFound(userNick)) throw UserNotFoundException(userNick)

        tweetRepository.save(Tweet(userNick,tweetText))
    }

    private fun userNotFound(userNick: String): Boolean {
        return userRepository.find(userNick) == null
    }


}