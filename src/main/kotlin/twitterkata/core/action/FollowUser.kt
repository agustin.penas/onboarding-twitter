package twitterkata.core.action

import twitterkata.core.domain.User
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class FollowUser(
    private val userRepository: UserRepository
) {
    fun execute(userNickName: String, friendNickName: String) {
        validateFriendExists(friendNickName)
        val user = findUser(userNickName)

        user.follow(friendNickName)
        save(user)

    }

    private fun findUser(nickname: String) = userRepository.find(nickname) ?: throw UserNotFoundException(nickname)

    private fun validateFriendExists(friendNickName: String) {
        findUser(friendNickName)
    }

    private fun save(user: User) {
        userRepository.save(user)
    }

}