package twitterkata.core.action

import twitterkata.core.domain.User
import twitterkata.core.domain.UserAlreadyExistsException
import twitterkata.core.domain.UserRepository

class RegisterUser(private val userRepository: UserRepository) {

    fun execute(username: String, nick: String) {
        if (userExists(nick)) throw UserAlreadyExistsException(nick)

        val user = User(username, nick)
        userRepository.save(user)

    }

    private fun userExists(nick: String): Boolean {
        return userRepository.find(nick) != null
    }
}