package twitterkata.core.action

import twitterkata.core.domain.TweetRepository
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class GetTweets(private val tweetRepository: TweetRepository, private val userRepository: UserRepository) {
    fun execute(nick: String): List<String> {
        if(userNotFound(nick)) throw UserNotFoundException(nick)
        return tweetRepository.find(nick)
    }

    private fun userNotFound(userNick: String): Boolean {
        return userRepository.find(userNick) == null
    }

}