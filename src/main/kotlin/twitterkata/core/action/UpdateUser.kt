package twitterkata.core.action

import twitterkata.core.domain.User
import twitterkata.core.domain.UserNotFoundException
import twitterkata.core.domain.UserRepository

class UpdateUser(private val userRepository: UserRepository) {

    fun execute(actionData: ActionData) {
        findUser(actionData.nickname).also { user ->
            updateRealName(user, actionData.newRealName)
            save(user)
        }
    }

    private fun findUser(nickname: String) = userRepository.find(nickname) ?: throw UserNotFoundException(nickname)

    private fun updateRealName(user: User, newRealName: String) {
        user.realName = newRealName
    }

    private fun save(user: User) {
        userRepository.save(user)
    }

    data class ActionData(val nickname: String, val newRealName: String)

}