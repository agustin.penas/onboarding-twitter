package twitterkata.core.domain

import java.lang.RuntimeException

class UserAlreadyExistsException(private val nick: String) : RuntimeException("El usuario $nick ya existe") {

}