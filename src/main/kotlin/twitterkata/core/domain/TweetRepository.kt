package twitterkata.core.domain

interface TweetRepository {

    fun save(tweet: Tweet)
    fun find(nick: String): List<String>
}
