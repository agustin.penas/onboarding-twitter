package twitterkata.core.domain

interface FollowingsRepository {
    fun save(userFollowingNick: String, userToFollowNick: String)
    fun followsOf(userWithFollows: String): List<String>
}