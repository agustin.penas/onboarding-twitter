package twitterkata.core.domain

data class Tweet(var userNick: String, var tweetText: String)