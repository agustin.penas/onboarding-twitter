package twitterkata.core.domain

import java.io.Serializable

class User(
    var realName: String,
    val nick: String,
    private val friends: MutableSet<String> = mutableSetOf()
) : Serializable {

    fun follow(friendNickName: String) {
        friends.add(friendNickName)
    }

    fun getFriends(): Set<String> {
        return friends
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (realName != other.realName) return false
        if (nick != other.nick) return false
        if (friends != other.friends) return false

        return true
    }

    override fun hashCode(): Int {
        var result = realName.hashCode()
        result = 31 * result + nick.hashCode()
        result = 31 * result + friends.hashCode()
        return result
    }

    override fun toString(): String {
        return "nick: $nick, name: $realName, friends: ${friends.toString()}"
    }


}
