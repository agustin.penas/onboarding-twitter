package twitterkata.core.domain

open interface UserRepository {

    fun save(user: User)
    fun find(nick: String): User?
}