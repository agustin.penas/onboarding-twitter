package twitterkata.core.domain

import java.lang.RuntimeException

class UserNotFoundException(val userNick: String) : RuntimeException("Usuario: $userNick no encontrado")