package twitterkata.infrastructure

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVRecord
import twitterkata.core.domain.User
import twitterkata.core.domain.UserRepository
import java.io.*
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


class FileUserRepository(private val fileName: String) : UserRepository {


    override fun save(user: User) {
        createFileIfNotExists()
        val csvParser = getCsvParser()
        val recordList = getAllRecords(csvParser, user)
        val csvPrinter = getCsvPrinter()

        saveRecords(recordList, csvPrinter)
        saveUser(user, csvPrinter)

        flushAndClose(csvPrinter)
    }

    override fun find(userNick: String): User? {
        if (fileDoesNotExists()) return null;

        val csvParser = getCsvParser()
        for (csvRecord in csvParser) {
            if (userFound(csvRecord, userNick))
                return createUser(csvRecord.get("Nick"), csvRecord.get("RealName"), csvRecord.get("Friends"))
        }
        return null
    }

    private fun createFileIfNotExists() {
        val file = File(fileName)
        if (!file.exists()) file.createNewFile();
    }

    private fun fileDoesNotExists(): Boolean {
        val file = File(fileName)
        return !file.exists()
    }

    private fun getCsvParser(): CSVParser {
        val reader = Files.newBufferedReader(Paths.get(fileName))
        return CSVParser(
            reader, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim()
        )
    }

    private fun getCsvPrinter(): CSVPrinter {
        val writer = Files.newBufferedWriter(Paths.get(fileName))
        return CSVPrinter(
            writer, CSVFormat.DEFAULT
                .withHeader("Nick", "RealName", "Friends")
        )
    }

    private fun userFound(csvRecord: CSVRecord, userNick: String): Boolean {
        return csvRecord.get("Nick") == userNick
    }

    private fun saveUser(user: User, csvPrinter: CSVPrinter) {
        val recordData = recordDataFromUser(user)
        csvPrinter.printRecord(recordData.nick, recordData.realname, recordData.friends)
    }

    private fun saveRecords(
        recordList: MutableList<RecordData>,
        csvPrinter: CSVPrinter
    ) {
        for (recordData in recordList) {
            csvPrinter.printRecord(recordData.nick, recordData.realname, recordData.friends)
        }
    }

    private fun getAllRecords(
        csvParser: CSVParser,
        user: User
    ): MutableList<RecordData> {
        val recordList = mutableListOf<RecordData>()
        for (csvRecord in csvParser) {
            var recordData = RecordData(csvRecord.get("Nick"), csvRecord.get("RealName"), csvRecord.get("Friends"))

            if (isNotTheUpdatingUser(recordData, user))
                recordList.add(recordData)
        }
        return recordList
    }

    private fun isNotTheUpdatingUser(
        recordData: RecordData,
        user: User
    ) = recordData.nick != user.nick

    private fun recordDataFromUser(user: User): FileUserRepository.RecordData {
        var friends = ""
        for (friend in user.getFriends())
            friends += "|$friend"
        return RecordData(user.nick, user.realName, friends)
    }

    private fun createUser(nick: String, realname: String, friendsString: String): User {
        val friendsSet = mutableSetOf<String>()
        val st = StringTokenizer(friendsString, "|")
        while (st.hasMoreTokens()) {
            val friend = st.nextToken()
            friendsSet.add(friend)
        }
        return User(realname, nick, friendsSet)
    }

    private fun flushAndClose(csvPrinter: CSVPrinter) {
        csvPrinter.flush()
        csvPrinter.close()
    }

    private data class RecordData(val nick: String, val realname: String, val friends: String)

}
