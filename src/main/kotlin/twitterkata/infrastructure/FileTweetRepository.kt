package twitterkata.infrastructure

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVRecord
import twitterkata.core.domain.Tweet
import twitterkata.core.domain.TweetRepository
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class FileTweetRepository(private val fileName: String) : TweetRepository{


    override fun save(tweet: Tweet) {
        createFileIfNotExists()
        val csvParser = getCsvParser()
        val recordList = getAllRecords(csvParser)
        val csvPrinter = getCsvPrinter()

        recordList.add(RecordData(tweet.userNick,tweet.tweetText))
        saveRecords(recordList, csvPrinter)

        flushAndClose(csvPrinter)
    }

    override fun find(userNick: String): List<String> {
        if (fileDoesNotExists()) return mutableListOf<String>();

        val csvParser = getCsvParser()
        val listOfTweets = mutableListOf<String>()
        for (csvRecord in csvParser) {
            if (userFound(csvRecord, userNick))
                listOfTweets.add(csvRecord.get("Tweet"))
        }
        return listOfTweets
    }

    private fun createFileIfNotExists() {
        val file = File(fileName)
        if (!file.exists()) file.createNewFile();
    }

    private fun fileDoesNotExists(): Boolean {
        val file = File(fileName)
        return !file.exists()
    }

    private fun getCsvParser(): CSVParser {
        val reader = Files.newBufferedReader(Paths.get(fileName))
        return CSVParser(
            reader, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim()
        )
    }

    private fun getCsvPrinter(): CSVPrinter {
        val writer = Files.newBufferedWriter(Paths.get(fileName))
        return CSVPrinter(
            writer, CSVFormat.DEFAULT
                .withHeader("Nick", "Tweet")
        )
    }

    private fun userFound(csvRecord: CSVRecord, userNick: String): Boolean {
        return csvRecord.get("Nick") == userNick
    }

    private fun saveRecords(
        recordList: MutableList<RecordData>,
        csvPrinter: CSVPrinter
    ) {
        for (recordData in recordList) {
            csvPrinter.printRecord(recordData.nick, recordData.tweet)
        }
    }

    private fun getAllRecords(csvParser: CSVParser): MutableList<RecordData> {
        val recordList = mutableListOf<RecordData>()
        for (csvRecord in csvParser)
            recordList.add(RecordData(csvRecord.get("Nick"), csvRecord.get("Tweet")))
        return recordList
    }



    private fun flushAndClose(csvPrinter: CSVPrinter) {
        csvPrinter.flush()
        csvPrinter.close()
    }

    private data class RecordData(val nick: String, val tweet: String)


}