package twitterkata.infrastructure

import twitterkata.core.domain.User
import twitterkata.core.domain.UserRepository

class InMemoryUserRepository : UserRepository {

    private val users = HashMap<String, User>()

    override fun save(user: User) {
        users[user.nick] = user
    }

    override fun find(nick: String): User? {
        if (!users.containsKey(nick)) return null
        return users[nick]
    }

}